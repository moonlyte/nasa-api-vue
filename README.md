# nasa-api-vue
By Jonas Østergaard den Hollander
##Introduction
A minimal project with the only purpose of serving interactive and readable data to the user. All data served belongs to the respective owners and this project is for educational purposes only.<br>
The API used for this project is the public Nasa images API. The project has features like: search, categorization, specific data detail, responsive and simple UI/UX.<br>
The project was made in a few hours spread over about a week.<br>
This project is not built for production usage and only for educational purposes.
## Requirements
This project was built with:
- node version: 15.0.4
- npm version: 7.0.15
## Commands
Install required packages
```
npm install
```
Run serve command
```
npm run serve
```
Run build command
```
npm run build
```

